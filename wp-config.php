<?php

// BEGIN iThemes Security - Do not modify or remove this line
// iThemes Security Config Details: 2
define( 'DISALLOW_FILE_EDIT', true ); // Disable File Editor - Security > Settings > WordPress Tweaks > File Editor
// END iThemes Security - Do not modify or remove this line

/**
 * La configuration de base de votre installation WordPress.
 *
 * Ce fichier contient les réglages de configuration suivants : réglages MySQL,
 * préfixe de table, clés secrètes, langue utilisée, et ABSPATH.
 * Vous pouvez en savoir plus à leur sujet en allant sur
 * {@link http://codex.wordpress.org/fr:Modifier_wp-config.php Modifier
 * wp-config.php}. C’est votre hébergeur qui doit vous donner vos
 * codes MySQL.
 *
 * Ce fichier est utilisé par le script de création de wp-config.php pendant
 * le processus d’installation. Vous n’avez pas à utiliser le site web, vous
 * pouvez simplement renommer ce fichier en "wp-config.php" et remplir les
 * valeurs.
 *
 * @package WordPress
 */

// ** Réglages MySQL - Votre hébergeur doit vous fournir ces informations. ** //
/** Nom de la base de données de WordPress. */
define('DB_NAME', 'wpcv');

/** Utilisateur de la base de données MySQL. */
define('DB_USER', 'root');

/** Mot de passe de la base de données MySQL. */
define('DB_PASSWORD', 'root');

/** Adresse de l’hébergement MySQL. */
define('DB_HOST', 'localhost');

/** Jeu de caractères à utiliser par la base de données lors de la création des tables. */
define('DB_CHARSET', 'utf8mb4');

/** Type de collation de la base de données.
  * N’y touchez que si vous savez ce que vous faites.
  */
define('DB_COLLATE', '');

/**#@+
 * Clés uniques d’authentification et salage.
 *
 * Remplacez les valeurs par défaut par des phrases uniques !
 * Vous pouvez générer des phrases aléatoires en utilisant
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ le service de clefs secrètes de WordPress.org}.
 * Vous pouvez modifier ces phrases à n’importe quel moment, afin d’invalider tous les cookies existants.
 * Cela forcera également tous les utilisateurs à se reconnecter.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'x0R!sHC@ZNr^frs@_{hujS!vg#|KUe$OFOJj=Fel-#dIj6(~(.8}@MI=f=(=td8p');
define('SECURE_AUTH_KEY',  'Tg`X66G9:qm.}aIrfD*)6|6jp$&/6$,.w!;L&cz:tr2ih;$&@-)ZKh{,2wG]FH%G');
define('LOGGED_IN_KEY',    'X%|}Kqh5`ak^;p9+;L-l5u4*/=]W ,p1Kt:>P+sP]80LI[|EI)9YIF;E[jyMn0v9');
define('NONCE_KEY',        'S8KwZYLar|<pB.1G.<,M|B3#@tjYMvjWCGXtdhI)OtAM=R%#y2;xY[PN~aT]<p3<');
define('AUTH_SALT',        'qje&-b>.ic#ka=R&t:?[*Gw6/8PK:xj]ZeZC}u]+V5V: N2F=X!~,owJvL%if5e.');
define('SECURE_AUTH_SALT', 'Rq0~Shv7d5c]hc@{{3mk~kR7{yoFx#ST mXv3Q%FJJ1Z,o!T!lM/]),};EHT/S+Q');
define('LOGGED_IN_SALT',   '8)5JUB^nnL6c@O -*|7Hd;W)j05Kx~7e-7fTILTjLZIiUv{wvYg[<LGcXpo!4{mj');
define('NONCE_SALT',       ')iz9}+=-_=l>k ;1Iu?&eK?gWa&?XYc$/,7t4g]qRB.jg<ST^KpnBk }%[>(snl#');
/**#@-*/

/**
 * Préfixe de base de données pour les tables de WordPress.
 *
 * Vous pouvez installer plusieurs WordPress sur une seule base de données
 * si vous leur donnez chacune un préfixe unique.
 * N’utilisez que des chiffres, des lettres non-accentuées, et des caractères soulignés !
 */
$table_prefix  = 'wpcv_';

/**
 * Pour les développeurs : le mode déboguage de WordPress.
 *
 * En passant la valeur suivante à "true", vous activez l’affichage des
 * notifications d’erreurs pendant vos essais.
 * Il est fortemment recommandé que les développeurs d’extensions et
 * de thèmes se servent de WP_DEBUG dans leur environnement de
 * développement.
 *
 * Pour plus d’information sur les autres constantes qui peuvent être utilisées
 * pour le déboguage, rendez-vous sur le Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* C’est tout, ne touchez pas à ce qui suit ! */

/** Chemin absolu vers le dossier de WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Réglage des variables de WordPress et de ses fichiers inclus. */
require_once(ABSPATH . 'wp-settings.php');